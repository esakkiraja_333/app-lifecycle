package com.ahaguru.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "Message";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i(TAG, "On Create Method is called");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "On Start Method is called");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(TAG, "On ReStart Method is called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "On Resume Method is called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "On Destroy Method is called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "On Pause Method is called");
    }
}